osm_where
=========

Simple tool to get [GeoURI](https://geouri.org/) for given
location identified by URL or by its name (not 100% reliable, as
we don’t have any interface to choose between multiple options).

Currently defaults to looking at Ukraine.

All issues, questions, complaints, or (even better!) patches
should be send via email to mcepl at cepl do eu (use [git
send-email](https://git-send-email.io/) for it).
